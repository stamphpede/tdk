# Stamphpede Test development kit

This repository contains all the dependencies you need to begin creating stamphpede
load tests.

We recommend that you do not use this repository directly, but instead use the
quickstart project. Advanced users may opt to ignore this advice. 
