<?php

namespace Stamphpede\Annotation;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
class Cacheable
{
    private bool $cacheable;

    public function __construct(array $cacheable)
    {
        $this->cacheable = (bool) $cacheable['value'];
    }

    public function isCacheable(): bool
    {
        return $this->cacheable;
    }
}
