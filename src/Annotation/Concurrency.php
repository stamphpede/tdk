<?php

namespace Stamphpede\Annotation;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
class Concurrency
{
    private int $max;

    public function __construct(array $max)
    {
        $this->max = (int) $max['value'];
    }

    public function getMax(): int
    {
        return $this->max;
    }
}
