<?php

namespace Stamphpede\Annotation;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
class Depends
{
    private string $on;

    public function __construct(array $on)
    {
        $this->on = $on['value'];
    }

    public function getOn(): string
    {
        return $this->on;
    }
}
