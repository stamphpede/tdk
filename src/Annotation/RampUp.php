<?php

namespace Stamphpede\Annotation;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
class RampUp
{
    private int $duration;

    public function __construct(array $duration)
    {
        $this->duration = (int) $duration['value'];
    }

    public function getDuration(): int
    {
        return $this->duration;
    }
}
