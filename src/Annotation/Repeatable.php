<?php

namespace Stamphpede\Annotation;

use Stamphpede\Parser\ParserException;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
class Repeatable
{
    public const REPEAT_OK = 'ok';
    public const REPEAT_END = 'end';
    public const REPEAT_IGNORE = 'ignore';
    public const REPEAT_SKIP = 'skip';
    public const REPEAT = [
        self::REPEAT_OK,
        self::REPEAT_END,
        self::REPEAT_IGNORE,
        self::REPEAT_SKIP,
    ];

    private string $behaviour;

    public function __construct(array $behaviour)
    {
        $this->behaviour = $behaviour['value'];
    }

    public function getBehaviour(): string
    {
        return $this->behaviour;
    }
}
