<?php

namespace Stamphpede\Annotation;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
class Task
{
    private string $name;

    public function __construct(array $name)
    {
        $this->name = $name['value'];
    }

    public function getName(): string
    {
        return $this->name;
    }
}
