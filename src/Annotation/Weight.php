<?php

namespace Stamphpede\Annotation;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
class Weight
{
    private int $weight;

    public function __construct(array $weight)
    {
        $this->weight = (int) $weight['value'];
    }

    public function getWeight(): int
    {
        return $this->weight;
    }
}
