<?php

namespace Stamphpede;

use Psr\Http\Message\RequestInterface;

class Request
{
    private string $taskName;
    private RequestInterface $psrRequest;

    public function __construct(string $taskName, RequestInterface $request)
    {
        $this->taskName = $taskName;
        $this->psrRequest = $request;
    }

    public function getPsrRequest(): RequestInterface
    {
        return $this->psrRequest;
    }

    public function getTaskName(): string
    {
        return $this->taskName;
    }

    public function getMethod(): string
    {
        return $this->psrRequest->getMethod();
    }

    public function getUrl(): string
    {
        return (string) $this->psrRequest->getUri();
    }
}
