<?php

namespace Stamphpede;

use Psr\Http\Message\ResponseInterface;

class Response
{
    private string $taskName;
    private ResponseInterface $response;

    public function __construct(string $taskName, ResponseInterface $response)
    {
        $this->taskName = $taskName;
        $this->response = $response;
    }

    public function getPsrResponse(): ResponseInterface
    {
        return $this->response;
    }

    public function getName(): string
    {
        return $this->taskName;
    }

    //@TODO add some additional helpers, possibly with a bit more defensiveness for bad responses.
    public function getBodyFromJson(): ?\stdClass
    {
        return json_decode($this->response->getBody());
    }

    public function hasFailed(): bool
    {
        return $this->hasServerError() || $this->hasClientError();
    }

    public function hasClientError(): bool
    {
        return $this->getStatusCode() >= 400 && $this->getStatusCode() < 500;
    }

    public function hasServerError(): bool
    {
        return $this->getStatusCode() >= 500;
    }

    public function getStatusCode(): int
    {
        return (int) $this->response->getStatusCode();
    }

}
