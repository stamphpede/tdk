<?php

namespace Stamphpede;

class ResponseCollection extends \ArrayObject
{
    public static function fromResponses(Response ...$responses): self
    {
        $instance = new self();
        foreach ($responses as $response) {
            $instance[$response->getName()] = $response;
        }

        return $instance;
    }
}
